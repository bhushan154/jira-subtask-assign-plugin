/*
Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
*/
package com.stygian.jira.plugins.postfunctions;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.mail.Email;
import com.atlassian.mail.server.MailServerManager;
import com.atlassian.mail.server.SMTPMailServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;
import java.util.Map;
import com.atlassian.jira.issue.MutableIssue;


/*
This is the post-function class that gets executed at the end of the transition.
Any parameters that were saved in your factory class will be available in the transientVars Map.
 */

public class NotifySubtaskAssignee extends AbstractJiraFunctionProvider{
    private static final Logger log = LoggerFactory.getLogger(NotifySubtaskAssignee.class);
    public static final String WATCHERS="watchers";
    public static final String ASSIGNEE="assignee";
    public static final String REPORTER="reporter";
    public static final String MESSAGE="message";

    public void execute(Map transientVars,Map args,PropertySet ps)throws WorkflowException{
        MutableIssue issue=getIssue(transientVars);

        MailServerManager mailServerManager = ComponentAccessor.getMailServerManager();
        SMTPMailServer mailServer = mailServerManager.getDefaultSMTPMailServer();

        if(!issue.isSubTask())
        {
            if(mailServer != null)
            {

                String watchers=(String)args.get(WATCHERS);
                String reporter=(String)args.get(REPORTER);
                String assignee=(String)args.get(ASSIGNEE);
                String message=(String)args.get(MESSAGE);


                    try
                    {
                        Email email = new Email(ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser().getEmailAddress());
                        StringBuilder emailTo = new StringBuilder();
                        for(Issue subtask:issue.getSubTaskObjects())
                        {
                            if(watchers.equals("true")){
                                for(User watches:ComponentAccessor.getWatcherManager().getCurrentWatchList(subtask,ComponentAccessor.getJiraAuthenticationContext().getLocale()))
                                {
                                    emailTo.append(watches.getEmailAddress() + ",");
                                }
                            }
                            if(assignee.equals("true"))
                            {
                                emailTo.append(subtask.getAssignee().getEmailAddress() + ",");
                            }
                            if(reporter.equals("true"))
                            {
                                emailTo.append(subtask.getReporter().getEmailAddress() + ",");
                            }
                        }
                        String toAddresses = emailTo.toString();
                        email.setTo(toAddresses);
                        email.setSubject(issue.getKey() + " updated.");
                        String content = new String();
                        if(message != null && message.equals(""))
                        {
                            content = message;
                        }
                        else
                        {
                            content = "The parent task " + issue.getKey() + " has been updated.";
                        }
                        email.setBody(content);
                        mailServer.send(email);
                    }
                    catch(Exception exc)
                    {
                        log.error("Error trying to send email to sub-task users. " + exc.toString());
                    }

            }
            else
            {
                log.error ("Error trying to send mail to sub-task users. No mail server configured.");
            }
        }
    }
}