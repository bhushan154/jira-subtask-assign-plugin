/*
Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
*/
package com.stygian.jira.plugins.postfunctions;

import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.opensymphony.workflow.loader.*;
import webwork.action.ActionContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
This is the factory class responsible for dealing with the UI for the post-function.
This is typically where you put default values into the velocity context and where you store user input.
 */

public class NotifySubtaskAssigneeFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginFunctionFactory{

    public static final String WATCHERS="watchers";
    public static final String ASSIGNEE="assignee";
    public static final String REPORTER="reporter";
    public static final String MESSAGE="message";
    private WorkflowManager workflowManager;


    public NotifySubtaskAssigneeFactory(WorkflowManager workflowManager){
            this.workflowManager=workflowManager;
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object>velocityParams){

        //the default message

    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object>velocityParams,AbstractDescriptor descriptor){

        getVelocityParamsForInput(velocityParams);
        getVelocityParamsForView(velocityParams, descriptor);

    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object>velocityParams,AbstractDescriptor descriptor){
        if(!(descriptor instanceof FunctionDescriptor))
        {
            throw new IllegalArgumentException("Descriptor must be a FunctionDescriptor.");
        }

        FunctionDescriptor functionDescriptor=(FunctionDescriptor)descriptor;

        String watchers=(String)functionDescriptor.getArgs().get(WATCHERS);
        String reporter=(String)functionDescriptor.getArgs().get(REPORTER);
        String assignee=(String)functionDescriptor.getArgs().get(ASSIGNEE);
        String message=(String)functionDescriptor.getArgs().get(MESSAGE);

        velocityParams.put(WATCHERS,watchers);
        velocityParams.put(REPORTER,reporter);
        velocityParams.put(ASSIGNEE,assignee);
        velocityParams.put(MESSAGE,message);
    }


    public Map<String,?>getDescriptorParams(Map<String, Object>formParams){
        Map params=new HashMap();

        // Process The map
        String watchers=extractSingleParam(formParams,WATCHERS);
        String reporter=extractSingleParam(formParams,REPORTER);
        String assignee=extractSingleParam(formParams,ASSIGNEE);
        String message=extractSingleParam(formParams,MESSAGE);
        params.put(WATCHERS,watchers);
        params.put(REPORTER,reporter);
        params.put(ASSIGNEE,assignee);
        params.put(MESSAGE,message);

        return params;
    }

}