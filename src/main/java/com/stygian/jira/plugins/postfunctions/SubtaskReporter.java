package com.stygian.jira.plugins.postfunctions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;
import java.util.Map;
import com.atlassian.jira.issue.MutableIssue;


/*
This is the post-function class that gets executed at the end of the transition.
Any parameters that were saved in your factory class will be available in the transientVars Map.
 */

public class SubtaskReporter extends AbstractJiraFunctionProvider{
    private static final Logger log = LoggerFactory.getLogger(SubtaskReporter.class);
    public static final String SUBTASK_ASSIGNEE="SUBTASK_ASSIGNEE";

    public void execute(Map transientVars,Map args,PropertySet ps)throws WorkflowException{
        MutableIssue issue=getIssue(transientVars);
        String assignTo = (String)args.get(SUBTASK_ASSIGNEE);

        if(issue.isSubTask())
        {
            if(assignTo.equals("assignee"))
            {
                if(issue.getParentObject().getAssigneeUser() != null)
                {
                    issue.setReporter(issue.getParentObject().getAssigneeUser());
                }
            }
            else if(assignTo.equals("reporter"))
            {
                if(issue.getParentObject().getReporterUser() != null)
                {
                    issue.setReporter(issue.getParentObject().getReporterUser());
                }
            }
            else if(assignTo.equals("projectlead"))
            {
                if(issue.getProjectObject().getLead() != null)
                {
                    issue.setReporter(issue.getProjectObject().getLead());
                }
            }
        }
    }
}