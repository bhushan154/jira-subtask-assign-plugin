/*
Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
*/
package com.stygian.jira.plugins.postfunctions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;
import java.util.Map;
import com.atlassian.jira.issue.MutableIssue;


/*
This is the post-function class that gets executed at the end of the transition.
Any parameters that were saved in your factory class will be available in the transientVars Map.
 */

public class SubtaskAssign extends AbstractJiraFunctionProvider{
    private static final Logger log = LoggerFactory.getLogger(SubtaskAssign.class);
    public static final String SUBTASK_ASSIGNEE="SUBTASK_ASSIGNEE";

    public void execute(Map transientVars,Map args,PropertySet ps)throws WorkflowException{
        MutableIssue issue=getIssue(transientVars);
        String assignTo = (String)args.get(SUBTASK_ASSIGNEE);

        if(issue.isSubTask())
        {
            if(assignTo.equals("assignee"))
            {
                if(issue.getParentObject().getAssigneeUser() != null)
                {
                    issue.setAssignee(issue.getParentObject().getAssigneeUser());
                }
            }
            else if(assignTo.equals("reporter"))
            {
                if(issue.getParentObject().getReporterUser() != null)
                {
                    issue.setAssignee(issue.getParentObject().getReporterUser());
                }
            }
            else if(assignTo.equals("projectlead"))
            {
                if(issue.getProjectObject().getLead() != null)
                {
                    issue.setAssignee(issue.getProjectObject().getLead());
                }
            }
        }
    }
}